CC=gcc
CFLAGS=-O3 -fopenmp

OBJ = stream_omp.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

stream: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)

.PHONY: clean

clean:
	rm -f *.o *~ core 
